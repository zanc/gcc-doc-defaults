Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: <ftp://ftp.gnu.org/gnu/gcc/gcc-4.9.1/gcc-4.9.1.tar.bz2>, notice
 that this package only contains several license files.
Comment:
 This package was put together by Nikita V. Youshchenko <yoush@debian.org>
 on Mon, 18 Sep 2006 00:34:35 +0400.
 .
 Copyright (C) 2006, Nikita V. Youshchenko <yoush@debian.org>

Files: gcc/doc/include/funding.texi gcc/doc/fsf-funding.7
Copyright: Copyright (c) 1994 Free Software Foundation, Inc.
License: GNU-meta-license-01
 Verbatim copying and redistribution of this section is permitted
 without royalty; alteration is not permitted.
Comment:
 This is basically a paraphrase of "GNU-meta-license", see the paragraph
 below. To accommodate for the textual difference, a different short name
 is used.

Files: gcc/doc/include/fdl.texi gcc/doc/gfdl.7
Copyright: Copyright (c) 2000, 2001, 2002, 2007, 2008 Free Software Foundation, Inc.
           http://fsf.org/
License: GNU-meta-license

Files: gcc/doc/include/gpl_v3.texi gcc/doc/gpl.7
Copyright: Copyright (c) 2007 Free Software Foundation, Inc.
License: GNU-meta-license

Files: debian/*
Copyright: Copyright (C) 2006, Nikita V. Youshchenko <yoush@debian.org>
           Copyright (C) 2012, 2013, 2014 Guo Yixuan <culu.gyx@gmail.com>
License: GPL-2

Files: info/*
Copyright: Copyright (C) 2014 Guo Yixuan <culu.gyx@gmail.com>
License: GPL-2

License: GNU-meta-license
 Everyone is permitted to copy and distribute verbatim copies of this
 license document, but changing it is not allowed.
Comment:
 GNU actually seems to offer more liberal terms for (parts of) these
 documents elsewhere; see the FAQ about the GNU licenses,
 <http://www.gnu.org/licenses/gpl-faq.html#ModifyGPL>.

License: GPL-2
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2"
